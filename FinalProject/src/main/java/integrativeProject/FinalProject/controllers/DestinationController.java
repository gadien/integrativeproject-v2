package integrativeProject.FinalProject.controllers;

import integrativeProject.FinalProject.beans.LoginResponse;
import integrativeProject.FinalProject.login.LoginManager;
import integrativeProject.FinalProject.services.interfaces.DestinationService;
import integrativeProject.FinalProject.services.interfaces.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("destination")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
public class DestinationController {
    private final String TAG = this.getClass().getSimpleName();
    private final DestinationService destinationService;
    private final LoginManager loginManager;

    public ResponseEntity<List<?>> getAllDestination(){
        return new ResponseEntity<>(destinationService.getAllDestinations(), HttpStatus.OK);
    }
}
